 resource "kubernetes_service" "wordpress" {
  metadata {
    name = "wordpress"

    labels = {
      app = "wordpress"
    }
  }

  spec {
    port {
      port        = 8080
    }

    selector = {
      app  = "wordpress"
      tier = kubernetes_replication_controller.wordpress.spec[0].selector.tier
    }

    type = "LoadBalancer"
  }
}

resource "kubernetes_storage_class" "wordpressd" {
  metadata {
    name = "wordpressd"
  }
  storage_provisioner = "kubernetes.io/gce-pd"
  parameters = {
    type = "pd-standard"
  }
}

resource "kubernetes_persistent_volume_claim" "wordpress" {
  metadata {
    name = "wp-pv-claim"

    labels = {
      app = "wordpress"
    }
  }

  spec {
    access_modes = ["ReadWriteOnce"]
    storage_class_name = "wordpressd" 
    resources {
      requests = {
        storage = "30Gi"
      }
    }
  }
}

resource "kubernetes_replication_controller" "wordpress" {
  metadata {
    name = "wordpress"

    labels = {
      app = "wordpress"
    }
  }

  spec {
    selector = {
      app  = "wordpress"
      tier = "frontend"
    }

    template {
      container {
        image = "wordpress:${var.wordpress_version}-apache"
        name  = "wordpress"

        env {
          name  = "WORDPRESS_DB_HOST"
          value = "wordpress-mysql"
        }

        env {
          name = "WORDPRESS_DB_PASSWORD"

          value_from {
            secret_key_ref {
              name = kubernetes_secret.mysql.metadata[0].name
              key  = "password"
            }
          }
        }

        port {
          container_port = 8080
          name           = "wordpress"
        }

        volume_mount {
          name       = "wordpress-persistent-storage"
          mount_path = "/var/www/html"
        }
      }

      volume {
        name = "wordpress-persistent-storage"

        persistent_volume_claim {
          claim_name = kubernetes_persistent_volume_claim.wordpress.metadata[0].name
        }
      }
    }
  }
}
