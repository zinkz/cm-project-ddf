To be able to create the k8s cluster, please amend the following variables to reflect your gcp environment: 
 - "project_id" & "region" in terraform.tfvars
 - "credentials" in vpc.tf
 - "wordpress", "mysql" versions and "mysql" passphrase in variables.tf

At this point in time, when you running "terraform init" and the subsequent commands "terraform plan" (dry-run of actual deployment) and terraform apply, the gcp k8s cluster along with its settings and configurations are created. When the gcp k8s cluster and the primary nodes are created, you will be presented with a connection timeout. This is because the k8s cluster is not yet created, and hence no pods could be deployed on the environment. Thus, you need to execute the k8s cluster command, found in the k8s engine secion on the GCP. After the cluster command is executed, you need to execute the "terraform apply" once again to continue with the deployment of:
 - 2x NGINX Webservers (acting as Proxies)
 - 1x Wordpress deployment
 - 1x mysql deployment.

*Note: The NGINX configuration file along with the Wordpress one were yet to be configured on this solution. However I was able to access both the NGINX Welcome Page and the Wordpress Installation Sites. Further the below configurations were still in progess:
 - Pass the correct configuration file (named default.conf) when deploying the NGINX Deployment using terraform to enable the proxy settings and having the NGINX web servers listening for client's requests. (amend the server name to listen to and enable the proxy setting "proxy pass")
 - Install certbot service on the NGINX deployment pods (Let's Encrypt) to generate SSL certificates for the web servers for 90 days and set the auto-renew cron job feature
 - Configure gcp storage service and upload a file to serve it on the wordpress site whenever someone access the site (ex: upload a Cisco Meraki AP datasheet on Google Cloud Storage, and when users access the site, the datasheet is fetched from the gcp cloud storage and presented to the user).
