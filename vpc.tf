variable "project_id" {
  description = "project id"
}

variable "region" {
  description = "region"
}

provider "google" {
  project = var.project_id
  region  = var.region
  credentials = file("gcp_cred_srvc.json")
}


output "region" {
  value       = var.region
  description = "region"
}
